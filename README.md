# My Apps

Here is the list of the projects that I have done or participate in, using Flutter Framework.

## Shop App
shop app helps stores in managing their products easily and customers in shopping from different stores  
### Features
 - Firebase Authentication
 - Cloud Firestore & Firebase Strorage
 - Clean Architecture
 - Bloc State Management
 - Simple and Beautiful UIs

 <p>
<img src="/uploads/80c52dc54dcb5209663054e696363ef0/shop_app_1.jpg" width="26%">
<img src="/uploads/9e14a1188b208a944437686adac5c950/shop_app_2.jpg" width="26%">
<img src="/uploads/86237a24e9f2d8b77aeb13a37e4049ce/shop_app_3.jpg" width="26%">
</p>

## Store UIs
 <p>
<img src="/uploads/2cee9bbf78a7fae7c2b50bf0545ec80a/shop_app_s1.jpg" width="26%">
<img src="/uploads/94e6d1d63ee2f138be442acc50434e0e/shop_app_s2.jpg" width="26%">
<img src="/uploads/3fb279cbf08f7d21f6f3aa9ea3431d5b/shop_app_s3.jpg" width="26%">
</p>

## Customer UIs
 <p>
<img src="/uploads/1ec7874b7269e6be05d3862a1dd749e1/shop_app_c1.jpg" width="26%">
<img src="/uploads/9c10fd8aaebc9d0982988a1202385944/shop_app_c2.jpg" width="26%">
<img src="/uploads/1d7427d63b8a0403ab7bd0601c5ae3d4/shop_app_c3.jpg" width="26%">
<img src="/uploads/cb652cbf1d8b775b23670f865bf525f3/shop_app_c4.jpg" width="26%">
<img src="/uploads/582ce0183b085061765a8d4b90694346/shop_app_c5.jpg" width="26%">

 </p>


## Plant Planet
This app helps in identify plants and plant's diseases from a photo and help users managing their garden
### Features
 - Clean Architecture
 - Bloc State Management
 - Restful Api
 - Local Caching

<p>
<img src="/uploads/d89075ee809d73c54303c3ba9836c352/Picture1.png" width="26%">
<img src="/uploads/e48058cd3d8b203e5f9e4d10387d7266/Picture2.png" width="26%">
<img src="/uploads/4c00e1af5a0f2681011531ee1d05711c/Picture3.png" width="26%">
<img src="/uploads/02c3fd079be31296e136bad43c20add4/Picture4.png" width="26%">
<img src="/uploads/45834b96b816db0c52488ec9b5f47e41/Picture5.png" width="26%">
<img src="/uploads/ab027e5a860a4384de0a5f9f16572446/Picture6.jpg" width="26%">

</p>

## Appointment App
an app that let users set a reminders for their doctors'appointments
### Features
 - Repository Pattern
 - Provider State Management
 - Shared Preferences for caching
 - Light and Dark Mode

<p>
<img src="/uploads/e90db335a6eb1558dade3145e74cfae8/app_light_1.jpg" width="26%">
<img src="/uploads/3be7a4e8ed847915845c65a204c64177/app_light_2.jpg" width="26%">
<img src="/uploads/a71f48d0391c97fe75dd88d448fa81f1/app_light_3.jpg" width="26%">
<img src="/uploads/f7a94b7b9248ffab5eb3eb62994b4b2a/app_light_4.jpg" width="26%">
<img src="/uploads/04a6147a8277ca8f359b343aba2b89d3/app_dark_1.jpg" width="26%">
<img src="/uploads/0f600d5103d896aaadd95951aaad7675/app_dark_2.jpg" width="26%">
<img src="/uploads/934f37033b5f6bed41224e8e34956895/app_dark_3.jpg" width="26%">
<img src="/uploads/911f3d36fdc173b3387be30a48cebf48/app_dark_4.jpg" width="26%">

</p>


## Todo App
a simple todo app with all CRUD operations
<p>
<img src="/uploads/cfcd746b565611fd40454091d5634cf6/todo_2.jpg" width="26%">
<img src="/uploads/3c3162e4aafda961599cb5388f95a89a/todo_1.jpg" width="26%">
</p>



## Weather App
a simple weather app to get the weather for a specific or current location

<p>
<img src="/uploads/ca59d3c76ec4475a4769ed500ccac947/weather_1.jpg" width="26%">
<img src="/uploads/fd7af3a605983838af739b59f250d3b7/weather_2.jpg" width="26%">
</p>

